package org.spring.data.cassandra.repository;

import org.spring.data.cassandra.model.Person;
import org.springframework.data.cassandra.repository.CassandraRepository;

public interface PersonRepository extends CassandraRepository<Person, String>{

}
