//package org.spring.data.cassandra.config;
//
//import java.util.Arrays;
//import java.util.List;
//
//import org.springframework.beans.factory.BeanClassLoaderAware;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.cassandra.SessionFactory;
//import org.springframework.data.cassandra.config.AbstractCassandraConfiguration;
//import org.springframework.data.cassandra.config.SchemaAction;
//import org.springframework.data.cassandra.core.cql.keyspace.CreateKeyspaceSpecification;
//import org.springframework.data.cassandra.core.cql.keyspace.DataCenterReplication;
//import org.springframework.data.cassandra.core.cql.keyspace.DropKeyspaceSpecification;
//import org.springframework.data.cassandra.core.cql.keyspace.KeyspaceOption;
//import org.springframework.data.cassandra.core.cql.session.init.CompositeKeyspacePopulator;
//import org.springframework.data.cassandra.core.cql.session.init.KeyspacePopulator;
//import org.springframework.data.cassandra.core.cql.session.init.ResourceKeyspacePopulator;
//import org.springframework.data.cassandra.core.cql.session.init.SessionFactoryInitializer;
//import org.springframework.data.cassandra.core.mapping.Embedded.Nullable;
//
//@Configuration
//public class CassandraConfiguration extends AbstractCassandraConfiguration implements BeanClassLoaderAware{
//	
//	@Value("${spring.data.cassandra.keyspace-name}")
//	private String KEYSPACE;
//	
//	@Value("${spring.data.cassandra.local-datacenter}")
//	private String DATACENTER;
//
//	@Override
//	protected String getKeyspaceName() {
//		return KEYSPACE;
//	}
//
//	@Override
//	protected String getLocalDataCenter() {
//		return DATACENTER;
//	}
//
//	@Override
//	protected List<CreateKeyspaceSpecification> getKeyspaceCreations() {
//		CreateKeyspaceSpecification specification = CreateKeyspaceSpecification.createKeyspace(KEYSPACE)
//				.ifNotExists()
//		        .with(KeyspaceOption.DURABLE_WRITES, true)
//		        .withSimpleReplication(1);
//	    return Arrays.asList(specification);
//	}
//
////	@Override
////	protected KeyspacePopulator keyspaceCleaner() {
////		return new ResourceKeyspacePopulator(scriptOf("DROP TABLE people"));
////	}
//
////	@Nullable
////	@Override
////	protected KeyspacePopulator keyspacePopulator() {
////		return new ResourceKeyspacePopulator(scriptOf("CREATE TABLE people(id text PRIMARY KEY, name text, age int)"));
////	}
//
////	@Override
////	protected List<DropKeyspaceSpecification> getKeyspaceDrops() {
////		return Arrays.asList(DropKeyspaceSpecification.dropKeyspace("my_keyspace"));
////	}
//	
//	@Bean
//	SessionFactoryInitializer sessionFactoryInitializer(SessionFactory sessionFactory) {
//		SessionFactoryInitializer initializer = new SessionFactoryInitializer();
//	    initializer.setSessionFactory(sessionFactory);
//	    
//	    ResourceKeyspacePopulator populator = new ResourceKeyspacePopulator(scriptOf("CREATE TABLE IF NOT EXISTS people(id text PRIMARY KEY, name text, age int)"));
//	    initializer.setKeyspacePopulator(populator);
//		return initializer;
//	}
////	
////	
////	@Override
////	  public String[] getEntityBasePackages() {
////	    return new String[] { "org.springframework.data.cassandra.model"};
////	  }
////
////	@Override
////	public SchemaAction getSchemaAction() {
////		return SchemaAction.CREATE_IF_NOT_EXISTS;
////	}
//	
//	
//	
//	
//	
//}
