package org.spring.data.cassandra;

import java.util.Random;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spring.data.cassandra.model.Person;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.data.cassandra.core.query.Criteria;
import org.springframework.data.cassandra.core.query.Query;

import com.datastax.oss.driver.api.core.CqlSession;

public class CassandraApplication {

  private static final Logger LOGGER = LoggerFactory.getLogger(CassandraApplication.class);

  protected static Person newPerson(String name, int age) {
    return new Person(UUID.randomUUID().toString(), name, age);
  }

  public static void main(String[] args) {

    CqlSession cqlSession = CqlSession.builder().withKeyspace("mykeyspace").build();
    
    CassandraOperations template = new CassandraTemplate(cqlSession);

    Person jonDoe = template.insert(newPerson("Lucifer Moningstar", 30));

    LOGGER.info(template.selectOne(Query.query(Criteria.where("id").is(jonDoe.getId())), Person.class).getId());

//    template.truncate(Person.class);
    cqlSession.close();
  }

}
