package org.spring.data.cassandra.resource;

import org.spring.data.cassandra.model.Person;
import org.spring.data.cassandra.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api")
public class PeopleResource {
	
	@Autowired
	private PersonRepository personRepository;
	
	@GetMapping(path = "/people")
	public ResponseEntity<List<Person>> getAll(){
		List<Person> response = personRepository.findAll();
		return ResponseEntity.ok(response);
	}

}
